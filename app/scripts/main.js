$('#html').on('click',function(){
	$(".html").css("display","block");  
	$(".css").css("display","none");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","none");
	$(".list").css("display","none");
	$(".position").css("display","none");
});

$('#css').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","block");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","none");
	$(".list").css("display","none");
	$(".position").css("display","none");
});

$('#java').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","none");  
	$(".java").css("display","block"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","none");
	$(".list").css("display","none");
	$(".position").css("display","none");
});

$('#header_content_footer').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","none");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","block");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","none");
	$(".list").css("display","none");
	$(".position").css("display","none");
});

$('#heading').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","none");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","block");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","none");
	$(".list").css("display","none");
	$(".position").css("display","none");
});

$('#margin_padding').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","none");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","block"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","none");
	$(".list").css("display","none");
	$(".position").css("display","none");
});

$('#breakpoint').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","none");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","block"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","none");
	$(".list").css("display","none");
	$(".position").css("display","none");
});

$('#container').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","none");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","block"); 
	$(".atribut").css("display","none");
	$(".list").css("display","none");
	$(".position").css("display","none");
});

$('#atribut').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","none");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","block");
	$(".list").css("display","none");
	$(".position").css("display","none");
});

$('#list').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","none");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","none");
	$(".list").css("display","block");
	$(".position").css("display","none");
});

$('#position').on('click',function(){
	$(".html").css("display","none");  
	$(".css").css("display","none");  
	$(".java").css("display","none"); 
	$(".header_content_footer").css("display","none");
	$(".heading").css("display","none");  
	$(".margin_padding").css("display","none"); 
	$(".breakpoint").css("display","none"); 
	$(".container").css("display","none"); 
	$(".atribut").css("display","none");
	$(".list").css("display","none");
	$(".position").css("display","block");
});


/*$('#click').on('click',function(){
	$(".click").css("display","none"); 
	$(".hide").css("display","block"); 
	$(".kotak_hide").css("display","block");
});

$('#hide').on('click',function(){
	$(".hide").css("display","none"); 
	$(".kotak_hide").css("display","none");
	$(".click").css("display","block"); 
});*/


function showclick(){
	document.getElementsByClassName("click")[0].style.display="none";
	document.getElementsByClassName("hide")[0].style.display="block";
	document.getElementsByClassName("kotak_hide")[0].style.display="block";
};

function hideclick(){
	document.getElementsByClassName("hide")[0].style.display="none";
	document.getElementsByClassName("kotak_hide")[0].style.display="none";
	document.getElementsByClassName("click")[0].style.display="block";
};